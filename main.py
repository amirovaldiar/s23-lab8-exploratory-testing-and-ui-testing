from selenium import webdriver, By

# Initialize driver
driver = webdriver.Chrome()

# Open website
driver.get('https://www.heroeswm.ru/')

# Username Input element
username_input = driver.find_element('name', 'login')
username_input.send_keys('USERNAME')

# Password Input element
password_input = driver.find_element('name', 'pass')
password_input.send_keys('PASSWORD')

# Click to Submit the login
driver.find_element('class name', 'entergame').click()

# Ensure that we are in
assert driver.title.startswith('Персонаж. HeroesWM.ru')

# Close the browser
driver.quit()
